resource "google_project_iam_member" "cloud_run_admin" {
  project = var.google_project
  role    = "roles/run.admin"
  member  = "serviceAccount:${google_service_account.training_account.email}"
}

resource "google_project_iam_member" "service_account_user" {
  project = var.google_project
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${google_service_account.training_account.email}"
}

resource "google_project_iam_member" "storage_admin" {
  project = var.google_project
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.training_account.email}"
}