resource "random_string" "account_suffix" {
  length           = 10
  special          = false
  number           = false
  lower            = true
  upper            = false
}

resource "google_service_account" "training_account" {
  account_id   = "terraform-training-${random_string.account_suffix.result}"
  display_name = "[DO NOT MODIFY MANUALLY]: Terraform training service account managed by GitLab CI/CD"
}

resource "google_service_account_key" "training_key" {
  service_account_id = google_service_account.training_account.name
}

resource "local_file" "training_credentials" {
    content     = base64decode(google_service_account_key.training_key.private_key)
    filename = "${path.module}/google-application-credentials.json"
}