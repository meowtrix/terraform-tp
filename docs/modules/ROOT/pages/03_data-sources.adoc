:experimental:
:checkpoint-sha: 0dca74f277b85204ad25b1481df4b4b0dc7f7ee3

= Data sources

**Covered features**: link:https://www.terraform.io/docs/language/data-sources/index.html[data sources], link:https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs/data-sources/image[`docker_image` source], link:https://www.terraform.io/docs/language/expressions/references.html#references-to-resource-attributes[referencing attributes]

include:::partial$checkpoint.adoc[]

== What is a _data source_ ?

Data sources allow `terraform` to use information defined outside of Terraform, defined by another separate Terraform configuration, or modified by functions.

A data source is accessed via a special kind of resource known as a data resource, declared using a data block:

[source,hcl]
----
data "docker_image" "nginx-img" { # <1>
    name = "nginx:latest" # <2>
}
----
<1> the `data` keyword to state a data source, then the type and a name
<2> In this case, the `docker_image` type only requires to name a docker image, which will be pulled from link:https://hub.docker.com[the `docker` hub]

Data resources cause `terraform` only to read objects and distinguish themselves from *resources* which are created, updated and/or deleted by `terraform`.


== Defining a _data source_

._That's good o' field for growing our potatoes! You be making it our source for crops!_
[quote, Colonel Hashicorp D.S]
image:potato-farm.jpg[]

Defining data sources is very straightforward. 

✏ Add the following lines in your `minio.tf` file created in the previous chapter: 

[.copy]
[source,hcl]
----
data "docker_image" "minio_img" {
  name = "minio/minio:alpine"
}
----

✏ Apply the new configuration: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply
----

(Output)

[source,bash]
----
docker_container.minio_tf: Refreshing state... [id=96c1530dba0a8299451cc53b5519f18c30ab68b41da691e33e30883a4344430e]
╷
│ Error: did not find docker image 'minio/minio:alpine'
│ 
│   with data.docker_image.minio_img,
│   on minio.tf line 1, in data "docker_image" "minio_img":
│    1: data "docker_image" "minio_img" {
│ 
╵
----

👀 You should notice an error stating that the `alpine` tag does not exist for the `minio` image.

NOTE: Since *_data sources_ are read-only* objects, if `terraform` is unable to retrieve the values when you apply the configuration, your deployment will fail

✏ Fix the configuration of the `minio_img` data source to use the latest `minio` image: 

[.copy]
[source,hcl]
----
data "docker_image" "minio_img" {
    name = "minio/minio:latest"
}
----

✏ Go ahead a try to apply the configuration again

[.copy]
[source,bash]
----
Λ\: $ terraform apply -auto-approve
----

(Output)

[source,bash]
----
docker_container.minio_tf: Refreshing state... [id=a955c20446853022b37e2b368152ac5e3871ca79b2c6ec20f7f1f052f76ac991]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # docker_container.minio_tf must be replaced
-/+ resource "docker_container" "minio_tf" {
      + bridge            = (known after apply)
      + container_logs    = (known after apply)
      - cpu_shares        = 0 -> null
      - dns               = [] -> null
      - dns_opts          = [] -> null
      - dns_search        = [] -> null
      ~ entrypoint        = [
          - "/usr/bin/docker-entrypoint.sh",
        ] -> (known after apply)
      + exit_code         = (known after apply)
      ~ gateway           = "172.17.0.1" -> (known after apply)
      - group_add         = [] -> null
      ~ hostname          = "a955c2044685" -> (known after apply)
      ~ id                = "a955c20446853022b37e2b368152ac5e3871ca79b2c6ec20f7f1f052f76ac991" -> (known after apply)
      ~ image             = "sha256:1afc478341fd5f3ca3c85e41ab97a04f81ec7560ee614ce5b85398f5cea811ad" -> "minio/minio:latest" # forces replacement
      ~ init              = false -> (known after apply)
      ~ ip_address        = "172.17.0.2" -> (known after apply)
      ~ ip_prefix_length  = 16 -> (known after apply)
      ~ ipc_mode          = "private" -> (known after apply)
      - links             = [] -> null
      - log_opts          = {} -> null
      - max_retry_count   = 0 -> null
      - memory            = 0 -> null
      - memory_swap       = 0 -> null
        name              = "minio-container"
      ~ network_data      = [
          - {
              - gateway                   = "172.17.0.1"
              - global_ipv6_address       = ""
              - global_ipv6_prefix_length = 0
              - ip_address                = "172.17.0.2"
              - ip_prefix_length          = 16
              - ipv6_gateway              = ""
              - network_name              = "bridge"
            },
        ] -> (known after apply)
      - network_mode      = "default" -> null
      - privileged        = false -> null
      - publish_all_ports = false -> null
      ~ security_opts     = [] -> (known after apply)
      ~ shm_size          = 64 -> (known after apply)
      - storage_opts      = {} -> null
      - sysctls           = {} -> null
      - tmpfs             = {} -> null
        # (13 unchanged attributes hidden)

      + healthcheck {
          + interval     = (known after apply)
          + retries      = (known after apply)
          + start_period = (known after apply)
          + test         = (known after apply)
          + timeout      = (known after apply)
        }

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }


      - volumes {
          - container_path = "/data" -> null
          - host_path      = "/absolute/path/to/supplies" -> null
          - read_only      = false -> null
        }
      + volumes {
          + container_path = "/data"
          + host_path      = "/absolute/path/to/supplies"
        }
        # (1 unchanged block hidden)
    }

Plan: 1 to add, 0 to change, 1 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: 
docker_container.minio_tf: Destroying... [id=6c9a20b5034a839c8ceebed18bf94a3164b58b138035baa99d804f664eac9fa7]
docker_container.minio_tf: Destruction complete after 1s
docker_container.minio_tf: Creating...
docker_container.minio_tf: Creation complete after 0s [id=953360006ed787aa154340af86aa232286507221463c4ce2c982b011e4ba6af4]

Apply complete! Resources: 1 added, 0 changed, 1 destroyed.
----

NOTE: As already stated in the previous chapter, the *docker container is forced to be recreated* because we defined the *image it needs using a name and tag*. During the deployment, this name is automatically converted to an ID which makes it look like the image was changed.  

As you can see, we still have our `minio_tf` container, but the data source can not be seen in the plan. This is because a _data source_ is not managed by `terraform`. It serves only as a source of information that can be required by other objects.

This is all looking good, but as we have defined a _data source_, we are still not using it for our `minio` container...

== Referencing attributes

It is time to create a dependency between the two objects you created, by referencing attributes from the _data source_ inside the _resource_.

✏ Update the `minio_tf` resource by changing it's `image` field with the following value: 

[.copy]
[source,hcl]
----
image = data.docker_image.minio_img.id # <1>
----
<1> This states that the value of the `image` parameter should be taken from the `id` attribute of the _data source_ of type `docker_image` called `minio_img`

✏ Go ahead and try to apply the new configuration

[.copy]
[source,bash]
----
Λ\: $ terraform apply
----

(Output)

[source,bash]
----
docker_container.minio_tf: Refreshing state... [id=953360006ed787aa154340af86aa232286507221463c4ce2c982b011e4ba6af4]

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
----

👀 As you can see, there is not much to do since you simply defined a _data source_ as an _input_ for your running container. Therefore `terraform` tells you the current deployed infrastructure already matches the one you have defined.

NOTE: In fact, we used the *`id` attribute* of the `docker_image` _data source_ to *properly define the `image`* used in the `docker_container` *with an ID* resource so `terraform` does not yell every time we apply the configuration. Hooray 🎊