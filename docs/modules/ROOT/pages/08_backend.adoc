:experimental:
:checkpoint-sha: 8b5972ad38f5cfc2a37d983b31270d6c041d1510

= Backend

**Covered features**: link:https://www.terraform.io/docs/language/settings/backends/index.html[backends], link:https://www.terraform.io/docs/language/settings/backends/local.html[local backend], link:https://www.terraform.io/docs/language/settings/backends/http.html[S3 backend] 

include:::partial$checkpoint.adoc[]

== What is a _backend_ ?

Each `terraform` configuration can specify a backend, which defines where and how operations are performed, where state snapshots are stored, etc...

Backend configuration documents the form of a backend block, which selects and configures a backend for a `terraform` configuration. There are several available but we'll be covering only two in this training: 

- The link:https://www.terraform.io/docs/language/settings/backends/local.html[local backend] which uses your local file system to store states as files

- The link:https://developer.hashicorp.com/terraform/language/settings/backends/s3[S3 backend] which uses AWS Cloud Storage to store information

NOTE: Up until now, you *have been using* the *local* backend as it is the *default behavior* of `terraform`

You usually define the configuration for your backend within your `main.tf` file using the `backend` keyword as follows:

[source,hcl]
----
terraform {
  backend "s3" {
    bucket = "MySuperBucket" # <1>
    key    = "path/to/my/file/in/the/bucket" # <2>
    region = "us-east-1"
  }

  # OR

  backend "local" { # <3>
       path = "relative/path/to/terraform.tfstate" # <4>
  }
}
----
<1> The S3 backend requires the name of the S3 bucket, which is unique worldwide 
<2> An optional prefix can be added to specify a subpath to store data
<3> The local backend can be configured to point to another directory
<4> By default, if boils down to the directory where you are running `terraform`

== Configuring the AWS S3 backend

._I am tired of constantly going from one restaurant to the other! Find me a way to run my business in my pyjamas!_
[quote, Colonel Hashicorp D.S]
image:colonel-pyjamas.jpg[]

Let's configure a AWS S3 backend shall we?

✏ Ask your trainer to send you the credentials you will need to give `terraform` so it can contact the S3 bucket that has already been created for you.

NOTE: You will be using a *user with just the rights you need* to connect to the backend. 

NOTE: The *credentials* consists in a *AWS shared credentials file*

✏ Ask your trainer to provide you the name of the bucket you shall use as your backend. It should be something close to `terraform-training-backend-h32qsdkq212s` where `h32qsdkq212s` is a random string.

[[_get_credentials_from_trainer]]
✏ Store those credentials somewhere safe on your computer, and *keep the path to this file*

✏ Modify the `main.tf` by adding a `backend` configuration section *in the `terraform` object* as follows: 

[.copy]
[source,hcl]
----
terraform {
  ...
  backend "s3" {
    bucket = "terraform-training-backend-h32qsdkq212s" # <1>
    key ="YOUR-NAME/" # <2>
    profile = "S3_backend_terraform"# <3>
    region = "eu-west-1"
  }
  ...
}
----
<1> Use the name of the bucket provided by your trainer
<2> Use your own full name separated with a dash `"-"` to avoid collisions with other attendees
<3> Replace profile by the one you have define in aws credentials file

[TIP]
====
You can *also use* the `AWS_ACCESS_KEY_ID` `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` *environment variables* with the path to the credentials: 

[.copy]
[source,bash]
----
Λ\: $ export AWS_ACCESS_KEY_ID=/path/to/the/credentials/access-key # <1>
Λ\: $ export AWS_SECRET_ACCESS_KEY=/path/to/the/credentials/secret-access-key # <1>
Λ\: $ export AWS_REGION='eu-west-1' # <1>
----
<1> Replace the path with your own
====


IMPORTANT: Since we *changed the `terraform` internal configuration*, we need to *reinitialize* it before going any further

✏ Reinitialize `terraform`: 

[.copy]
[source,bash]
----
Λ\: $ terraform init
----

(Output)

[source,bash]
----
Initializing the backend...
Do you want to copy existing state to the new backend?
  Pre-existing state was found while migrating the previous "local" backend to the
  newly configured "s3" backend. No existing state was found in the newly
  configured "s3" backend. Do you want to copy this state to the new "s3"
  backend? Enter "yes" to copy and "no" to start with an empty state.

  Enter a value:
----

👀 As you can see, `terraform` asks you if you want to migrate your current state to the remote state since you changed its internal configuration

✏ Go ahead: type `yes` and press kbd:[Enter]

[source,bash]
----
Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Reusing previous version of kreuzwerker/docker from the dependency lock file
- Reusing previous version of hashicorp/random from the dependency lock file
- Using previously-installed kreuzwerker/docker v2.15.0
- Using previously-installed hashicorp/random v3.1.0

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
----

✏ You can now safely delete your local `terraform.tfstate` file and the `terraform.tfstate.d` folder: 

[.copy]
[source,bash]
----
Λ\: $ rm -rf ./terraform.tfstate*
----

== Using the remote backend

✏ Try to make a change of your choice in the configuration, create a new workspace, or destroy the current one (if not the default one), and then run: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply
----

👀 It should work seamlessly as if your state was local, though it's remote!

[TIP]
====
To have a look at what your state files look like in the bucket, either ask your trainer to show you or install the link:https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html[`aws` command line] tool, and run the following: 

[.copy]
[source,bash]
----
Λ\: $ aws configure 
AWS Access Key ID [****************IPEK]: Access Key ID # <1> 
AWS Secret Access Key [****************pboz]: Secret Access Key # <1> 
Default region name []: eu-west-1 
Default output format []: json 
----
<1> Replace this with your own Access Key ID,Secret Access Key

[.copy]
[source,bash]
----
Λ\: $ aws s3 ls terraform-training-backend-h32qsdkq212s/YOUR-NAME/ # <1>
----
<1> Replace `YOUR-NAME` with the prefix you used in your `main.tf`

(Output)

[source,bash]
----
2023-02-28 18:09:25          0 
2023-02-28 18:26:06       7840 terraform.tfstat
----
====

[NOTE]
====
If you already have `aws` CLI installed, run the following to avoid having conflicts with your existing identities when running gsutil:

[.copy]
[source,bash]
----
Λ\: $ aws configure --profile S3_backend_terraform
AWS Access Key ID [****************IPEK]: Access Key ID # <1> 
AWS Secret Access Key [****************pboz]: Secret Access Key # <1> 
Default region name []: eu-west-1 
Default output format []: json 
----
<1> Replace this with your own Access Key ID,Secret Access Key
====
